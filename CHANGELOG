# Changes from airac-16-07 to airac-16-
1. Fixed AC North static boundary, adding previously missed lines - thanks to @hsugden (Harry Sugden)
2. Added AC West sector static boundary - thanks to @hsugden (Harry Sugden)
3. Added Ronaldsway (EGNS) VRPs - thanks to @agent_squash (Alex Ashley)
4. Amended agreement between Heathrow INT S and TC SW and added CPT radar gate - thanks to @hsugden (Harry Sugden)
5. AIRAC (1608) - Liverpool (EGGP) RNAV approach fixes added - thanks to @agent_squash (Alex Ashley)
6. Added squawk ranges to various London ACC event only splits - thanks to @hsugden (Harry Sugden)
7. Added La Manche delegated airspace as ARTCC High data - thanks to @hsugden (Harry Sugden)
8. AIRAC (1607) - Belfast (EGAA) runway coordinates and 07/25 extended centreline updated - thanks to @hsugden (Harry Sugden)
9. AIRAC (1608) - Kirkwall (EGPA) RNAV approach fixes added - thanks to @hsugden (Harry Sugden)
10. AIRAC (1609) - Barra (EGPR), Islay (EGPI) and Land's End (EGHC) RNAV approach fixes added - thanks to @hsugden (Harry Sugden)
11. AIRAC (1609) - Birmingham (EGBB) Runway 15 2Y RNAV SIDs now permanent procedure - thanks to @hsugden (Harry Sugden)
12. AIRAC (1609) - Southampton (EGHI) radar frequency updated to 122.720 - thanks to @hsugden (Harry Sugden)
13. Sector Line, Ownership and Agreement changes relating to TC East and TC South airspace changes - thanks to @hsugden (Harry Sugden)
14. Added EGPK, EGAE, EGPE and EGNC GND/TWR/APP sector rings to display to Area - thanks to @hsugden (Harry Sugden)
15. AIRAC (1609) - Shoreham (EGKA) runway track and coordinate updates - thanks to @hsugden (Harry Sugden)
16. AIRAC (1609) - N552 and N562 now RNAV routes, and N552 route updated - thanks to @hsugden (Harry Sugden)

# Changes from airac-16-06 to airac-16-07
1. Adjusted default visibility centre and 'scaling' - thanks to @jpfox (Jamie Fox)
2. Added missing Scottish TMA 5 Class D airspace base label - thanks to @hsugden (Harry Sugden)
3. Fixed ordering of Birmingham (EGBB) RWY 33 DTY SIDs - thanks to @hsugden (Harry Sugden)
4. Fixed longitudinal indicator in Gatwick (EGKK) KKE10 coordinate - thanks to @hsugden (Harry Sugden)
5. Added EGNX GND/TWR sector rings to display to Area - thanks to @hsugden (Harry Sugden)
6. Added EGBB GND/TWR sector rings to display to Area - thanks to @hsugden (Harry Sugden)
7. Added KEGUN arrivals to EGNR Stars.txt file - thanks to @hsugden (Harry Sugden)
8. Added missing Newcastle (EGNT) ground position - thanks to @hsugden (Harry Sugden)
9. Added EGNT GND/TWR sector rings to display to Area - thanks to @hsugden (Harry Sugden)
10. Added missing Biggin (EGKB) [RNAV] STARs and transitions - thanks to @agent_squash (Alex Ashley)
11. Added missing Cranfield (EGTC) RNAV fix 'DOTIT' - thanks to @agent_squash (Alex Ashley)
12. AIRAC (1607) - Benbecula (BEN) VOR/DME now DME only - thanks to @hsugden (Harry Sugden)
13. AIRAC (1607) - Barrow/Walney Island (EGNL) RNAV approaches and associated airfield fixes - thanks to @agent_squash (Alex Ashley)
14. AIRAC (1607) - Campbeltown (EGEC) and Wick (EGPC) RNAV approaches and associated airfield fixes - thanks to @hsugden (Harry Sugden)
15. AIRAC (1607) - Carlisle (EGNC) runway track and coordinate updates - thanks to @hsugden (Harry Sugden)
16. AIRAC (1513) - Removed Belfast (EGAA) ground position - thanks to @hsugden (Harry Sugden)
17. Added EGAA TWR/APP sector rings to display to Area - thanks to @hsugden (Harry Sugden)
18. Added new Newquay (EGHQ) ground map - thanks to @agent_squash (Alex Ashley)

# Changes from airac-16-03 to airac-16-06
1. Fixed Antrim callsign suffix, fixing ownership/position identification - thanks to @hsugden (Harry Sugden)
2. Fixed erroneous longitudinal prefix in KKS17 (BOGNA/HARDY EGKK SIDs) - thanks to @hsugden (Harry Sugden)
3. Updated LTC sector line display, particularly for LTC_SW - thanks to @hsugden (Harry Sugden)
4. AIRAC - Added BUZAD and WOBUN to UL10 airway - thanks to @lpeters (Luke Peters)
5. Updated EGWU threshold coordinates and magnetic headings - thanks to @trevorhannant
6. Improved Essex COPX prediction (Luton Gate and SSINT->FIN) - thanks to @samw (Sam White)
7. Added Worthing-Paris release line as 'STAR' data in sector file - thanks to @pierr3 (Pierre Ferran)
8. AIRAC (1605) - Various Jersey (EGJJ) SIDs realigned and redesignated - thanks to @acrix (Matthew Moy)
9. Added Southend (EGMC) VRPs - thanks to @tasosp (Tasos Petropoulos)
10. AIRAC (1604) - Scatsta (EGPM) Radar frequency updated - thanks to @tasosp (Tasos Petropoulos)
11. AIRAC (1606) - L15/UL15/N859 routing changes - thanks to @lpeters (Luke Peters)

# Changes from airac-16-02 to airac-16-03
1. Added EG R220 - thanks to @acriX (Matthew Moy)
2. Military runway designator updates (includes EGOV/QL/QS/VN) - thanks to @trevorhannant
3. Removed geo lines from JACKO/GURLU to the start of the EGLC point merge arc - thanks to @hsugden (Harry Sugden)
4. Added correct sector ownership for ENSV and EKDK thus also fixing some line display - thanks to @hsugden (Harry Sugden)
5. Removed remaining references to Manston (EGMH) in agreements and in adjacent airport setup - thanks to @hsugden (Harry Sugden)
6. Added EG R219 - thanks to @samw (Sam White)
7. Added EGGP and EGPH GND/TWR/APP sector rings to display to Area - thanks to @lpeters (Luke Peters)
8. Added EGCC GND/TWR/APP sector rings to display to Area - thanks to @hsugden (Harry Sugden)
9. Added adjacent airports ownership for Scottish positions (000A Show adjacent departure airports) - thanks to @hsugden (Harry Sugden)
10. Corrected Stornoway (EGPO) top-down priority - thanks to @hsugden (Harry Sugden)
11. Added FL245 shelf/portion of the EGPX/EISN FIR - thanks to @hsugden (Harry Sugden)
12. Fixed Lossiemouth/Kinloss MATZ airspace to fit ARTCC low naming conventions - thanks to @hsugden (Harry Sugden)
13. Added EGPF GND/TWR/APP sector rings to display to Area - thanks to @lpeters (Luke Peters)
14. Added Southern CTA bases and moved Clacton CTA base - thanks to @lpeters (Luke Peters)
15. Added missing fixes on Gatwick STARs (TIMBA1J/1K/3F/2G) - thanks to @lpeters (Luke Peters)
16. Added EGPD GND/TWR/APP sector rings to display to Area - thanks to @lpeters (Luke Peters)
17. Added LTC SE and SW sector static boundaries - thanks to @samw (Sam White)
18. Added AC Daventry sector static boundary - thanks to @hsugden (Harry Sugden)
19. Added LTC NE and NW sector static boundaries - thanks to @hsugden (Harry Sugden)
20. Reformatted RMA whitespace - thanks to @samw (Sam White) and @hsugden (Harry Sugden)
21. Added missing Y3 airspace base (to the east of the EGGD CTA) - thanks to @hsugden (Harry Sugden)
22. Added EGPD COPX agreements - thanks to @lpeters (Luke Peters)
23. Added Essex Radar to Stansted Director automatic handoff - thanks to @samw (Sam White)
24. Added missing agreements to/from PEAPP - thanks to @hsugden (Harry Sugden)
25. Removed Manchester stand 50 - thanks to @acrix (Matthew Moy)
26. AIRAC - Removed Manchester holding point W2 - thanks to @acrix (Matthew Moy)
27. Adjusted CC INT/FIN sectors and inbound agreements with MAN - thanks to @hsugden (Harry Sugden)
28. Added AC Dover sector static boundary - thanks to @hsugden (Harry Sugden)
29. Added AC Clacton sector static boundary - thanks to @hsugden (Harry Sugden)
30. Added AC Worthing sector static boundary - thanks to @hsugden (Harry Sugden)
31. Updated French CTRs, added the Lille CTR, and updated the Deauville and Lille TMAs - thanks to @pierr3 (Pierre Ferran)
32. AIRAC - CAE NDB withdrawn; fix FERAS withdrawn; EG D323A danger area updated - thanks to @hsugden (Harry Sugden)
33. AIRAC - EGHC RWY 07 RNAV approach and associated airfield fixes - thanks to @acrix (Matthew Moy)
34. Adjusted and renamed EGCC RMAs, including a single 4 mile ring centred on the ARP - thanks to @acrix (Matthew Moy)
35. Added missing agreements between DUBN and Antrim, particularly for airway Y911 - thanks to @hsugden (Harry Sugden)
